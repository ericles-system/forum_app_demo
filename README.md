# FORUM DEMO APP

## TABLE OF CONTENTS

- [Introduction](#introduction)
- [Development](#development)
- [How running and unit testing API](#run)

<a name="introduction"></a>
## INTRODUCTION

Forum Demo app for questions and answers on various technology issues

<a name="development"></a>
## DEVELOPMENT

### Backend: Python Flask-Restful - API

>[black](https://black.readthedocs.io/en/stable/)

>[pre-commit](https://pre-commit.com/)

>[guicorn Python WSGI HTTP Server for UNIX](https://gunicorn.org/)

>[flake8](http://flake8.pycqa.org/en/latest/)

>[Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/)

>[Loguru](https://github.com/Delgan/loguru)

>[Dynaconf - Easy and Powerful Settings Configuration for Python](https://dynaconf.readthedocs.io/en/latest/)

>[Dataset: databases for lazy people](https://dataset.readthedocs.io/en/latest/)

Tree

    .
    ├── docker-compose.yaml
    ├── dockerfile
    ├── README.md
    ├── requirements.txt
    └── src
        ├── app.py
        ├── common
        │   ├── dao
        │   │   ├── answer_query.py
        │   │   ├── __init__.py
        │   │   ├── likes_query.py
        │   │   ├── model.py
        │   │   ├── questions_query.py
        │   │   ├── tags_query.py
        │   │   └── user_query.py
        │   └── utils
        │       ├── enums.py
        │       ├── helpers.py
        │       ├── __init__.py
        │       ├── loggins.py
        │       └── logutils.py
        ├── config.py
        ├── resources
        │   ├── answer.py
        │   ├── __init__.py
        │   ├── likes.py
        │   ├── _ping.py
        │   ├── question.py
        │   └── tags.py
        ├── routes
        │   └── __init__.py
        ├── settings.toml
        └── tests.py

### Front-end: ReactJS + Redux

>[Library React JS](https://pt-br.reactjs.org/docs/getting-started.html)

>[Redux is a predictable state container for JavaScript apps.](https://redux.js.org/introduction/core-concepts)

>[Redux Middleware: Multi, Promise](https://redux.js.org/api/applymiddleware)

>[Thunk middleware for Redux](https://github.com/reduxjs/redux-thunk)

>[React Bootstrap](https://react-bootstrap.github.io/)

>[Axios is a lightweight HTTP client based on the $http](https://alligator.io/react/axios-react/)

Tree

    .
    ├── Dockerfile
    ├── package.json
    ├── public
    │   ├── favicon.ico
    │   ├── favicon.png
    │   ├── index.html
    │   ├── manifest.json
    │   └── robots.txt
    ├── README.md
    └── src
        ├── actions
        │   ├── answer-add-actions.js
        │   ├── answer-paginated-actions.js
        │   ├── question-add-actions.js
        │   ├── question-paginated-actions.js
        │   ├── service-counter-actions.js
        │   └── tags-action.js
        ├── App.css
        ├── App.js
        ├── App.test.js
        ├── assets
        │   └── images
        │       ├── forum-logo.jpg
        │       └── ribbon-tag.png
        ├── commons
        │   ├── content
        │   │   ├── content.css
        │   │   └── content.jsx
        │   ├── footer
        │   │   ├── footer.css
        │   │   └── footer.jsx
        │   ├── forum-select
        │   │   ├── forum-select.css
        │   │   └── forum-select.jsx
        │   ├── header
        │   │   ├── header.css
        │   │   └── header.jsx
        │   ├── interceptors
        │   │   └── service-counter-interceptor.js
        │   ├── layout
        │   │   ├── layout.css
        │   │   └── layout.jsx
        │   ├── pagination
        │   │   ├── pagination.css
        │   │   └── pagination.jsx
        │   ├── service-counter-overlay
        │   │   ├── service-counter-overlay.css
        │   │   └── service-counter-overlay.jsx
        │   ├── spinner
        │   │   └── spinner.jsx
        │   ├── storage
        │   │   └── storage.js
        │   ├── task-bar
        │   │   ├── task-bar.css
        │   │   ├── task-bar-item.css
        │   │   ├── task-bar-item.jsx
        │   │   └── task-bar.jsx
        │   └── utils.js
        ├── components
        │   ├── answer
        │   │   ├── answer.css
        │   │   ├── answer-form
        │   │   │   ├── answer-form.css
        │   │   │   └── answer-form.jsx
        │   │   ├── answer.jsx
        │   │   └── answer-list
        │   │       ├── answer-list.css
        │   │       └── answer-list.jsx
        │   ├── question
        │   │   ├── question.css
        │   │   ├── question.jsx
        │   │   └── question-list
        │   │       ├── question-list.css
        │   │       └── question-list.jsx
        │   ├── question-add
        │   │   ├── question-add.css
        │   │   ├── question-add.jsx
        │   │   └── question-form
        │   │       ├── question-form.css
        │   │       └── question-form.jsx
        │   └── tags-select
        │       └── tags-select.jsx
        ├── index.css
        ├── index.js
        ├── logo.svg
        ├── reducers
        │   ├── answer-add-reducer.js
        │   ├── answer-paginated-reducer.js
        │   ├── index.js
        │   ├── question-add-reducer.js
        │   ├── question-paginated-reducer.js
        │   ├── service-counter-reducer.js
        │   └── tags-reducer.js
        ├── serviceWorker.js
        └── store
            └── index.js

### Database MySQL

Schema of database

![Schema of database](./assets/images/schema_db.png)

<a name="run"></a>
## RUN

- **Prerequisites**

    Docker version >= 19.03.8
    
    Docker compose version >= 1.25.1


>NOTE: Case you have mysql client installed, make stop down of service: `sudo systemctl stop mysql`

```
docker-compose up
```

>Running Frontend in: http://127.0.0.1:80/

>Running API in: http://127.0.0.1:8000

Clean up containers services

```
docker-compose down
```


### UNIT TESTING API

- **Prerequisites**

    Container of forum-api-container needs to be running

```bash
# access container
docker exec -it forum-api-container bash
# after
python tests.py
```




