from common.utils import helpers, loggins, logutils
from config import FLASK_DEBUG, FLASK_HOST, FLASK_PORT
from dynaconf import FlaskDynaconf
from flask import Flask
from flask.helpers import locked_cached_property
from flask_cors import CORS
from routes import api_bp


class LoggerFlask(Flask):
    @locked_cached_property
    def logger(self):
        return loggins.logger


class MyConfig(object):
    RESTFUL_JSON = {"cls": helpers.CustomJSONEncoder}  # add whatever settings here

    @staticmethod
    def init_app(app):
        app.config["RESTFUL_JSON"]["cls"] = app.json_encoder = helpers.CustomJSONEncoder
        app.config["PROFILE"] = False


app = Flask(__name__)
app.config["BUNDLE_ERRORS"] = True
app.config.from_object(MyConfig)
MyConfig.init_app(app)
FlaskDynaconf(app)
CORS(app)

# blue print
app.register_blueprint(api_bp, url_prefix="/api")


@app.before_request
def add_log_request_id():
    extra = {"request_id": logutils.request_id()}
    loggins.logger.configure(extra=extra)


@app.after_request
def add_request_id_header(response):
    extra = {"request_id": None}
    loggins.logger.configure(extra=extra)
    response.headers["X-Request-ID"] = logutils.request_id()
    return response


if __name__ == "__main__":
    # from werkzeug.contrib.profiler import ProfilerMiddleware
    # app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[50])
    app.run(host=FLASK_HOST, port=int(FLASK_PORT), debug=FLASK_DEBUG)
