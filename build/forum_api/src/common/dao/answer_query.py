import dataset
from common.utils.loggins import logger
from config import LOGGER_LEVEL, MYSQL_URL

from .model import ModelBase


class AnwserDAO(ModelBase):
    def __init__(self):
        self.result = []
        self.result_obj = {}
        self.loglevel = True if LOGGER_LEVEL.upper() == "DEBUG" else False
        self.total = 0

    def read_all(self, parameters):
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel}) as db:
            params = {
                "page_size": parameters["page_size"],
                "offset": parameters["offset"],
                "question_id": parameters["question_id"],
            }
            query = """
                SELECT
                    a.id,
                    a.text,
                    u.name as userName,
                    a.created_in as createdIn,
                    a.`like`,
                    (
                        SELECT
                            COUNT(a.id)
                        FROM answers a
                        JOIN question q ON a.question_id = q.id
                        JOIN `user` u on a.user_id = u.id
                        WHERE
                            q.id = :question_id
                    ) as total
                FROM answers a
                JOIN question q ON a.question_id = q.id
                JOIN `user` u on a.user_id = u.id
                WHERE
                    q.id = :question_id
                ORDER BY
                    a.created_in DESC
                LIMIT :page_size
                OFFSET :offset;
            """
            data = db.query(query, params)
            for d in data:
                self.total = d.pop("total")
                self.result.append(d)

    def create(self, parameters):
        db = dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel})
        db.begin()
        try:
            answer_id = db["answers"].insert(parameters)
            self.result_obj = db["answers"].find_one(id=answer_id)
            db.commit()
        except Exception as e:
            db.rollback()
            logger.exception(f"error to create question, rollback was performed: ", exec_info=True)
            raise Exception(f"was not possible to create, rollback was performed: {str(e)}")
