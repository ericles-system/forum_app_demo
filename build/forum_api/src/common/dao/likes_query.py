import dataset
from common.utils.loggins import logger
from config import LOGGER_LEVEL, MYSQL_URL

from .model import ModelBase


class LikesDAO(ModelBase):

    _loglevel = True if LOGGER_LEVEL.upper() == "DEBUG" else False

    def __init__(self):
        self.result = []
        self.result_obj = {}
        self.loglevel = LikesDAO._loglevel

    def read_all(self, parameters=None):
        pass

    def create(self, parameters):
        pass

    def update(self, parameters):
        like = 0
        db = dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel})
        db.begin()
        try:
            if parameters["type"] == "questions":
                like = self.get_likes(parameters["id"], "question", db)["like"]
                like = 1 if like == 0 else like + 1
                print(like)
                data = dict(id=parameters["id"], like=like)
                db["question"].update(data, ["id"])
                self.result_obj = db["question"].find_one(id=parameters["id"])
            else:
                like = self.get_likes(parameters["id"], "answers", db)["like"]
                like = 1 if like == 0 else like + 1
                data = dict(id=parameters["id"], like=like)
                db["answers"].update(data, ["id"])
                self.result_obj = db["answers"].find_one(id=parameters["id"])

            db.commit()
        except Exception as e:
            db.rollback()
            logger.exception(f"error to update liked, rollback was performed: ", exec_info=True)
            raise Exception(f"was not possible to update liked, rollback was performed: {str(e)}")

    @staticmethod
    def get_likes(id, table, db):
        return db[table].find_one(id=id)
