import dataset
import pendulum
from common.utils.loggins import logger
from config import LOGGER_LEVEL, MYSQL_URL

from .model import ModelBase


class QuestionDAO(ModelBase):

    loglevel = True if LOGGER_LEVEL.upper() == "DEBUG" else False

    def __init__(self):
        self.result = []
        self.result_obj = {}
        self.total = 0
        self.loglevel = QuestionDAO.loglevel

    def read_all(self, parameters):
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel}) as db:
            now = parameters.pop("now")
            parameters.pop("page")
            where, params = self.mount_where(parameters)
            query = f"""
                SELECT
                    q.id,
                    q.title,
                    q.text,
                    u.name as userName,
                    q.created_in as createdIn,
                    q.`like`,
                    (
                        SELECT
                            COUNT(q.id) as total
                        FROM question q
                        JOIN `user` u on q.user_id = u.id
                        {where}
                    ) as total,
                    (
                        SELECT
                            COUNT(a.id)
                        FROM answers a
                        WHERE
                            a.question_id = q.id
                    ) as answerTotal
                FROM question q
                JOIN `user` u on q.user_id = u.id
                {where}
                ORDER BY
                    q.created_in DESC
                LIMIT :page_size
                OFFSET :offset;
            """
            data = db.query(query, params)
            for d in data:
                self.total = d.pop("total")
                d["interval"] = self.cal_interval(now, d["createdIn"])
                d["tags"] = self.get_tags(d["id"], db)
                title = d.pop("title")
                self.result.append({"title": title.capitalize(), **d})

    def create(self, parameters):
        db = dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel})
        db.begin()
        try:
            tags = parameters.pop("tags")
            question_id = db["question"].insert(parameters)
            for t in tags:
                t["question_id"] = question_id

            db["tags_question"].insert_many(tags)

            self.result_obj = db["question"].find_one(id=question_id)
            db.commit()
        except Exception as e:
            db.rollback()
            logger.exception(f"error to create question, rollback was performed: ", exec_info=True)
            raise Exception(f"was not possible to create, rollback was performed: {str(e)}")

    @classmethod
    def get_question(cls, question_id):
        result = {}
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": QuestionDAO.loglevel}) as db:
            params = {
                "question_id": question_id,
            }
            query = """
                SELECT
                    q.id,
                    q.title,
                    q.text,
                    u.name as userName,
                    q.`like`,
                    q.created_in as createdIn
                FROM question q
                JOIN `user` u on q.user_id = u.id
                WHERE
                    q.id = :question_id
            """
            try:
                r = next(db.query(query, params))
                result = {
                    "id": r["id"],
                    "title": r["title"].capitalize(),
                    "text": r["text"],
                    "userName": r["userName"],
                    "like": r["like"],
                    "createdIn": r["createdIn"],
                    "tags": cls.get_tags(question_id, db),
                }
            except StopIteration:
                pass

        return result

    @staticmethod
    def cal_interval(now, created_ind):
        last_status_update = pendulum.instance(created_ind, "UTC")
        interval = (now - last_status_update).total_seconds() / 60
        return interval

    @staticmethod
    def get_tags(question_id, db):
        params = {
            "question_id": question_id,
        }
        query = """
            SELECT
                t.id as tag_id,
                t.`name` as tag
            FROM question q
            JOIN tags_question tq on tq.question_id = q.id
            JOIN tags t on tq.tag_id = t.id
            WHERE
                q.id = :question_id;
        """
        tags = db.query(query, params)
        return [d for d in tags]

    @staticmethod
    def mount_where(data):
        params = {
            "page_size": data["page_size"],
            "offset": data["offset"],
            "title": data["title"],
        }
        where = "#"
        if data["title"]:
            params["title"] = "%" + data["title"] + "%"
            where = """
                WHERE
                    lower(q.`title`) LIKE :title
            """
        return where, params
