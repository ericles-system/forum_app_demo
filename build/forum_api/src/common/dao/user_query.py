import dataset
from config import LOGGER_LEVEL, MYSQL_URL

from .model import ModelBase


class UsersDAO(ModelBase):

    _loglevel = True if LOGGER_LEVEL.upper() == "DEBUG" else False

    def __init__(self):
        self.result = []
        self.loglevel = UsersDAO._loglevel

    def read_all(self, parameters=None):
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": self.loglevel}) as db:
            data = db["user"].all()
            for d in data:
                self.result.append(d)

    @classmethod
    def count_users(cls):
        count = 5
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": UsersDAO._loglevel}) as db:
            try:
                count = next(db.query("SELECT COUNT(id) as total FROM user;"))["total"]
            except StopIteration:
                pass

        return count

    def create(self, parameters):
        pass
