""" Enums """

ERROR = {"message": None}
LIST = {"data": []}
LIST_PAGED = {"total": None, "data": []}
OBJ = {}


def response(action, **kargs):
    """
        Default response API

        Arguments:
            actions {error, list or list_paged}

        **kargs
            - msg
            - data
            - total
            - object
    """
    try:
        if action == "error":
            ERROR["message"] = kargs.get("msg")
            return ERROR
        elif action == "list":
            LIST["data"] = kargs.get("data")
            return LIST
        elif action == "list_paged":
            LIST_PAGED["data"] = kargs.get("data")
            LIST_PAGED["total"] = kargs.get("total")
            return LIST_PAGED
        elif action == "object":
            return kargs.get("object")
        else:
            return ERROR
    except (Exception, KeyError) as e:
        print(f"response failed {str(e)}")
        return ERROR
