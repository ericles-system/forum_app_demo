import dataset
from config import MYSQL_URL
from flask_restful import Resource


class PingStatus(Resource):
    def get(self):
        # with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": True}) as db:
        #     table = db['user']
        #     table.insert({
        #             "name": "Eric Niens",
        #             "email": "eric.niens@e2mcorp.com",
        #             "password": "abc123",
        #             "cell_phone": "11966333366"
        #         })
        #         # result = db.query("DESC user")
        #         # for r in result:
        #         #     print(r['Field'])
        us = []
        with dataset.connect(url=MYSQL_URL, engine_kwargs={"echo": True}) as db:
            users = db["user"].all()
            for u in users:
                us.append(u)

        data = {"status": "OK"}
        return data, 200
