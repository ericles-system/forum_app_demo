from common.dao.answer_query import AnwserDAO
from common.dao.questions_query import QuestionDAO
from common.utils import helpers
from common.utils.enums import response
from common.utils.loggins import logger
from flask import request
from flask_restful import Resource, reqparse


class Answers(Resource):
    def get(self, id=None, question_id=None):
        if not id and question_id:
            data = dict()
            data["page"] = 1 if int(request.args.get("page")) <= 0 else int(request.args.get("page"))
            data["page_size"] = 50 if int(request.args.get("page_size")) <= 0 else int(request.args.get("page_size"))
            offset = data["page_size"] * (data["page"] - 1)
            data["offset"] = offset
            data["question_id"] = question_id

            a = AnwserDAO()
            q = QuestionDAO.get_question(question_id)
            a.read_all(data)
            q["answers"] = a.result

            body = response("list_paged", data=q, total=a.total)
            logger.info("answers were returned successfully!")
            return body, 200

        body = response("list", data=[])
        return body, 500

    def post(self, question_id=None):
        post_parser = reqparse.RequestParser()
        post_parser.add_argument(
            "text", required=True, dest="text", type=str, location="json", help="Error field: {error_msg}",
        )
        args = post_parser.parse_args()
        args["user_id"] = helpers.generate_user_id()
        args["question_id"] = question_id

        try:
            a = AnwserDAO()
            a.create(args)
        except Exception as e:
            body = response("error", msg=str(e))
            return body, 500

        logger.info("created answer successfully!")
        return a.result_obj, 201
