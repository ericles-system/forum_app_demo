from common.dao.likes_query import LikesDAO
from common.utils.enums import response
from common.utils.loggins import logger
from flask_restful import Resource, reqparse


class Likes(Resource):
    def put(self):
        put_parser = reqparse.RequestParser()
        put_parser.add_argument(
            "type",
            dest="type",
            choices=("questions", "answers"),
            required=True,
            type=str,
            location="json",
            help="Error field: {error_msg}",
        )
        put_parser.add_argument(
            "id", dest="id", required=True, type=int, location="json", help="Error field: {error_msg}",
        )

        args = put_parser.parse_args()

        try:
            like = LikesDAO()
            like.update(args)
        except Exception as e:
            body = response("error", msg=str(e))
            return body, 500

        obj = {args["type"]: like.result_obj}
        body = response("object", object=obj)
        logger.info("update like successfully!")
        return body, 200
