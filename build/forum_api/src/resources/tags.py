from common.dao.tags_query import TagsDAO
from common.utils.enums import response
from common.utils.loggins import logger
from flask_restful import Resource


class Tags(Resource):
    def get(self, id=None):
        if not id:
            q = TagsDAO()
            q.read_all()
            body = response("list", data=q.result)
            logger.info("tags were returned successfully!")
            return body, 200

        body = response("list", data=[])
        return body, 500
