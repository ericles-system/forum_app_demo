-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: forumdb
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `created_in` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_in` timestamp NULL DEFAULT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `like` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_answers_answers1_idx` (`question_id`),
  KEY `fk_answers_user_idx` (`user_id`),
  CONSTRAINT `fk_answers_question1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`),
  CONSTRAINT `fk_answers_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'Vá em painel de controle do notebook/Central de Rede e Compartilhamento/Alterar as Configurações do adaptador/selecione e \n            clique com o botão direito em cada um dos ícones de redes que aparecerem e veja se o notebook está servindo de ponte para o \n            celular. Dá uma boa investigada nessas redes, analise as conexões de ponte, desative e ative cada uma, faça um diagnóstico, etc., \n            se deletar a rede não tem problema é só criar outra, aliás até convém.\n\n			Se não funcionar:\n\n			Faça um reset no roteador.\n			Atualize o driver de rede.\n			Se precisar repita o procedimento nas configurações do adaptador\n			Reinicie a máquina\n			Outra alternativa:\n\n			Vá em Iniciar / Programas / Acessórios / Prompt de Comando / botão direito\n\n			Executar como Administrador /\n\n			digite: ipconfig/release (enter)\n\n			digite: ipconfig/renew (enter)\n\n			digite: ipconfig/flushdns (enter)\n\n			digite: netsh winsock reset (enter)\n\n			Reinicie a máquina','2020-05-14 03:47:17',NULL,1,2,0),(2,'Adobe Reader é ruidoso e vulnerável!\n			Desinstala e apaga os diretórios. depois instala o PDF Reader pegando direto da Microsoft Store, \n            que comigo resolveu!','2020-05-14 03:47:17',NULL,2,2,0),(3,'Amigo! Boa tarde! Também estou com o mesmo problema! Conseguiu resolver já? \n            Se sim me informe por favor ! Abraço!','2020-05-14 03:47:17',NULL,3,2,0),(4,'Pra quem tem idéias de como melhorar, entrem em contato com eles e questionem sobre a viabilidade \n            de atenderem seus pedidos:\n\n			https://www.whatsapp.com/contact/','2020-05-14 03:47:17',NULL,4,3,0),(5,'Querer que aplicativo de comunicação funcione sem rede é o mesmo que querer usar um \n            aparelho elétrico sem eletricidade kkkk','2020-05-14 03:47:17',NULL,4,4,0),(6,'Acho que falta função de moderadores em grupos do app para auxiliar os donos!','2020-05-14 03:47:17',NULL,4,5,0),(7,'por isso que disse,pra jogar gta rp 8 gb é mt pouco,disseram que é 16, por isso minha dificuldade,\n            agr é trabalhar msm pra conseguir comprar esse pente de 8 gb ai :( , vlw mano','2020-05-14 03:47:17',NULL,5,1,29),(8,'melhor jogar GTA com rp 12GB é bem melhor!!','2020-05-14 03:47:17',NULL,5,5,0),(9,'Acredito que você tenha que ativar o DHCP também no repetidor, pois nem sempre os repetidores conseguem usar o DHCP do roteador principal, o DHCP que faz a distribuição de IPs.\n\n			Mas quais são os modelos do roteador e do repetidor Wifi?','2020-05-17 05:35:13',NULL,6,4,0),(10,'Também acho que é na conexão com a rede elétrica como afirmou o @makrov12, as conexões HDMI são mais mais sensíveis, então verifica os seguintes detalhes:\n\n			Caso a TV tenha tomada de 3 Pinos e você usou uma adaptador para ignorar o fio terra, a polaridade também é importante, verifique se a polaridade não está invertida, Neutro no lugar da fase.\n\n			O Decoder da TV a cabo geralmente tem tomada de 2 pinos e inverte a polaridade automaticamente.','2020-05-17 05:50:41',NULL,7,5,1),(11,'Voce aguarda e testa mais tarde, nos primeiros dias pouquissimas pessoas conseguiram consultar a situação do auxilio.\n\nProvavelmente hoje voce consegue.','2020-05-18 05:53:02',NULL,18,4,2),(12,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:24',NULL,18,10,2),(13,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:32',NULL,18,3,3),(14,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:33',NULL,18,7,0),(15,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:34',NULL,18,6,4),(16,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:35',NULL,18,10,0),(17,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:37',NULL,18,6,0),(18,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:38',NULL,18,6,3),(19,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:39',NULL,18,2,0),(20,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:40',NULL,18,5,0),(21,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:41',NULL,18,4,0),(22,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:44',NULL,18,2,0),(23,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:45',NULL,18,10,0),(24,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:46',NULL,18,6,0),(25,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:47',NULL,18,9,0),(26,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:47',NULL,18,1,0),(27,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:47',NULL,18,3,0),(28,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:47',NULL,18,5,0),(29,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:48',NULL,18,8,0),(30,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:48',NULL,18,8,0),(31,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:48',NULL,18,9,0),(32,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:48',NULL,18,5,0),(33,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:48',NULL,18,2,0),(34,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:49',NULL,18,6,13),(35,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:49',NULL,18,7,0),(36,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:49',NULL,18,10,0),(37,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:49',NULL,18,9,0),(38,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:49',NULL,18,7,0),(39,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:50',NULL,18,2,12),(40,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:50',NULL,18,3,2),(41,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-18 05:54:50',NULL,18,1,1),(42,'Oi, Rodrigo!\n\nO TechTudo também tem uma matéria sobre dicas para se proteger dos golpes envolvendo o novo coronavírus.\n\nhttps://www.techtudo.com.br/listas/2020/04/golpes-no-whatsapp-prometem-auxilio-emergencial-saiba-como-se-proteger.ghtml','2020-05-18 05:58:01',NULL,18,9,4),(43,'Oi, Rodrigo!\n\nO TechTudo também tem uma matéria sobre dicas para se proteger dos golpes envolvendo o novo coronavírus.\n\nhttps://www.techtudo.com.br/listas/2020/04/golpes-no-whatsapp-prometem-auxilio-emergencial-saiba-como-se-proteger.ghtml','2020-05-18 05:58:06',NULL,18,2,5),(44,'Oi, Rodrigo! O TechTudo também tem uma matéria sobre dicas para se proteger dos golpes envolvendo o novo coronavírus. https://www.techtudo.com.br/listas/2020/04/golpes-no-whatsapp-prometem-auxilio-emergencial-saiba-como-se-proteger.ghtml','2020-05-18 22:47:14',NULL,18,6,5),(45,'Tenho outra duvida\n\nMais gostaria de sanar depois\n5488','2020-05-18 22:49:48',NULL,18,10,13),(46,'Vou ser o primeiro a responder.','2020-05-18 22:51:43',NULL,8,5,0),(47,'Vou ser o segundo agora a responder','2020-05-18 22:52:06',NULL,8,3,0),(48,'provavelmente é o drive da placa de internet!','2020-05-19 00:40:19',NULL,14,2,0),(49,'provavelmente é o drive da placa de internet!','2020-05-19 00:40:34',NULL,14,5,0),(50,'provavelmente é o drive da placa de internet!','2020-05-19 00:40:36',NULL,14,2,0),(51,'provavelmente é o drive da placa de internet!','2020-05-19 00:40:38',NULL,14,2,0),(52,'provavelmente é o drive da placa de internet!','2020-05-19 00:40:40',NULL,14,2,0),(53,'provavelmente é o drive da placa de internet!','2020-05-19 00:40:47',NULL,14,7,0),(54,'Energia elétrica é sempre complicada kkkk','2020-05-19 05:52:51',NULL,9,5,2),(55,'Por que algum pendrive, download, site.. que você acessou pode iniciar a execução de um programa indesejado!','2020-05-19 05:59:19',NULL,5,3,0),(56,'Resposta do test unitario','2020-05-19 19:46:55',NULL,5,3,0),(57,'Resposta do test unitario','2020-05-19 19:48:08',NULL,5,2,0),(59,'Resposta do test unitario','2020-05-19 23:48:52',NULL,5,7,0),(60,'Eu estou com um problema curioso no computador da empresa.\n\nA tecla do CAPS LOCK está invertida.\n\nCom o teclado com caps lock ativado, só sai os caracteres minúsculos. Com a tecla desativada [luz apagada], só aparecem os caracteres maiúsculos.\n\nDeveria ser o contrário, não? Como se resolve isso?','2020-05-20 00:13:28',NULL,25,5,2),(61,'Esta solução merecia um Óscar, tendo em consideração todas as outras \"soluções teóricas\" que vi antes de encontrar esta, que me permitiu resolver, este problema, em \"5\" segundos! Obrigado! :)','2020-05-20 00:16:54',NULL,25,9,0),(62,'Maravilha. Simples e direto, um Ippon em 2 segundos. Obrigado.','2020-05-20 00:17:21',NULL,25,3,0),(63,'Que coisa sinistra, parece Easter Egg!','2020-05-20 00:20:41',NULL,25,5,2),(64,'Esta solução merecia um Óscar','2020-05-20 00:21:42',NULL,25,5,0),(65,'Esta solução merecia um Óscar','2020-05-20 00:22:29',NULL,25,8,0),(66,'Esta solução merecia um Óscar','2020-05-20 00:23:41',NULL,25,5,4),(67,'Esta solução merecia um Óscar','2020-05-20 00:25:56',NULL,25,6,0),(68,'Esta solução merecia um Óscar','2020-05-20 00:26:23',NULL,25,4,1),(69,'e encontrar esta, que me permitiu resolver, este problema, em \"5\" segundos!','2020-05-20 00:26:40',NULL,25,8,1),(70,'Esta solução merecia um Óscar 555','2020-05-20 00:27:26',NULL,25,10,0),(71,'Bom dia . Quando o código está dando inválido, o que fazer ?','2020-05-20 00:31:38',NULL,26,10,0),(72,'pois é! eu tô tentando achar mas tem vários com o mesmo nome... qual é o aplicativo do governo de verdade?','2020-05-20 00:31:46',NULL,26,7,3),(73,'Busca da Play Store pelo aplicativo do auxílio emergencial: https://s3.glbimg.com/v1/AUTH_c3907a0b1bc34e0195281f8cc56fbda7/forum_techtudo/2020/04/07/7f335cf4-78e7-11ea-a290-0242ac110004-WhatsApp_Image_2020-04-07_at_12.48.09.jpeg\n\nTem um bando de aplicativo falso no meio...','2020-05-20 00:33:20',NULL,26,5,1);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `text` text NOT NULL,
  `created_in` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_in` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `like` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_question_user_idx` (`user_id`),
  CONSTRAINT `fk_question_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'Wi-Fi no celular só funciona quando o do notebook está ligado.','O Wi-fi do meu celular (G4 Play) só funciona quando o Wi-fi do notebook (Inspiron 11 3000)\n        tá ligado. Sempre que desligo o notebook, o Wi-fi do celular só volta a funcionar algumas horas\n        depois ou se desligar e ligar o roteador. Isso só acontece com o meu celular. O computador e o celular \n        do meu tio continuam funcionando normalmente. Não posso ficar tirando toda hora o roteador da tomada porque \n        meu tio trabalha no computador. Já está muito chato ter que deixar o notebook ligado pra usar o celular ou nem \n        ligar o notebook. Se alguém puder me ajudar, ficarei agradecido.','2020-05-14 03:47:17',NULL,1,0),(2,'Problema ao abrir arquivos PDF','Olá , estou com um problema em relação a abrir os arquivos em PDF , \n        antes disso acontecer meu programa padrão para abrir era o Adobe , pensei que seria algum problema nele , \n        testei outros e aconteceu a mesma coisa , nem pelos navegadores estou conseguindo abrir os documentos em PDF , \n        alguém poderia me ajudar caso já tenham visto algo parecido ?','2020-05-14 03:47:17',NULL,1,0),(3,'Problemas para reset no Xiaomi Airdots','Estou precisando dar reset nos Airdots (problema no volume, já vi que é muito simples resolver, \n        basta resetá-ló). O problema é: não consigo dar reset. Pelas instruções, basta apertar o botão por cerca de \n        vinte segundos até as luzes vermelha e branca piscarem duas vezes simultaneamente, porém, quando faço isso, \n        ele apenas “liga” (indicando com som e com led) sucessivas vezes.','2020-05-14 03:47:17',NULL,1,0),(4,'WhatsApp: qual função ainda falta no app?','Oi, pessoal.\n\n		O WhatsApp tem atualizações recorrentes, mas sempre sinto falta de alguns recursos. Gostaria de funções como: ter como descobrir a localização dos meus contatos, saber quando as pessoas estão em outras conversas e poder usar o WhatsApp sem precisar de internet.\n\n		No meu Android ou no WhatsApp Web, tudo isso seria muito útil.\n\n		E vocês, o que gostariam de ver no mensageiro?','2020-05-14 03:47:17',NULL,2,0),(5,'RAM COM MEMORIAS MUITO DISTINTAS FERRAM O PC?','eu comprei um pc gamer a tempos atrás e nunca tive problema com pouca ram(8 gb) mas \n        agora jogando gta rp tive problemas ao jogar por causa da ram,me sugeriram comprar outro pente de 8 \n        mas está fora do orçamento,dai eu tinha uns computadores antigos e fui ver e achei um pente ddr3(igual o meu) \n        porém com frequencia 1333mhz,o pente que tenho é hiperX e acredito ser de pela casa de 2400,e caso não haja \n        problemas em colocar essas frequencias distintas como faço pra testar?pq no trampo do meu pai pedi a ele que \n        olhasse pra ver se tinha mais ram lá,pois o pente que achei aqui é 2 gb e está guardado a mt tempo,tenho \n        medo de danificar meu pc','2020-05-14 03:47:17',NULL,3,0),(6,'Repetidor não conecta pelo celular, fica aguardando IP. Como resolver?','Tenho internet banda larga em minha casa com roteador wifi fornecido pelo provedor. Comprei um repetidor WiFi Repeater para melhor o sinal no segundo andar. Consigo fazer toda a configuração básica do repetidor e o mesmo funciona perfeitamente pelo notebook. Porém quando tento acessar pelo celular ele fica tentando conectar até dar erro no qual fala que não foi possível obter o IP. Já testei em outros aparelhos e a mesma coisa acontece, fica tentando obter o IP. Já verifiquei o canal utilizado e o mesmo não está congestionado. Preciso de ajuda pois não conheço tecnicamente deste assunto.','2020-05-17 05:35:10',NULL,1,0),(7,'TV PISCA QUANDO USO ENERGIA ELÉTRICA','Desde que passei a conectar o aparelho de tv a cabo na tv através de cabo HDMI sempre que uso energia na sala (tipo acender o interruptor de luz ou ligar o ventilador) a tela apaga e segundos depois volta normalmente como se tivesse perdido o sinal...\n\nIsso é problema na rede elétrica??? ou na tv? ou no cabo?\n\nPS: Quando usava cabo RCA (aquele amarelo, branco e vermelho) isso nunca aconteceu...','2020-05-17 05:50:37',NULL,3,7),(8,'TV pisca quando uso energia elétrica...','Desde que passei a conectar o aparelho de tv a cabo na tv através de cabo HDMI sempre que uso energia na sala (tipo acender o interruptor de luz ou ligar o ventilador) a tela apaga e segundos depois volta normalmente como se tivesse perdido o sinal... Isso é problema na rede elétrica??? ou na tv? ou no cabo? PS: Quando usava cabo RCA (aquele amarelo, branco e vermelho) isso nunca aconteceu...','2020-05-17 17:03:35',NULL,2,0),(9,'TV pisca quando uso energia elétrica...','Desde que passei a conectar o aparelho de tv a cabo na tv através de cabo HDMI sempre que uso energia na sala (tipo acender o interruptor de luz ou ligar o ventilador) a tela apaga e segundos depois volta normalmente como se tivesse perdido o sinal... Isso é problema na rede elétrica??? ou na tv? ou no cabo? PS: Quando usava cabo RCA (aquele amarelo, branco e vermelho) isso nunca aconteceu...','2020-05-17 17:05:09',NULL,2,7),(11,'Unum sumiu do play store','Não localizo o UNUM mais no Play Store','2020-05-17 19:21:59',NULL,2,0),(12,'Dúvida na compra de um computador novo','Peço um tempo de vcs para me ajudar.\n\nEstou procurando um PC razoável para jogar jogos populares e possivelmente jogos futuros (não tão pesados, e nem em máxima qualidade)\n\nAcabei encontrando esses, mas não sei muito bem qual escolher, se puderem me ajudar e dizer o por quê, agradeço:','2020-05-17 19:28:38',NULL,2,0),(13,'Como fazer para ativar a função de \"efeitos\" em chat de videochamada no instagram?','O ícone \"Efeitos\" não aparece em videochamada no meu Instagram, tanto em chamadas individuais ou chat em grupo. Já atualizei a versão do Instagram e o Android do meu celular S9. O que pode ser?','2020-05-18 01:23:26',NULL,2,0),(14,'Resolução do monitor mudando sozinho, o que pode ser?','Boa tarde, tenho um monitor meio antigo e que recentemente começou a apresentar um problema, toda vez que entro em jogos com resolução 1024x768 e depois saio do jogo ele continua na resolução 1024x768 e não tem jeito de voltar pra nativa (1366x768), só consigo voltar ao normal reiniciando o pc pois nem indo nas configurações de vídeo eu consigo fazer a mudança. Tenho uma placa de vídeo nvidia geforce 1050 ti e todos os drivers estão atualizados, inclusive o da placa de vídeo.','2020-05-18 04:36:38',NULL,2,0),(16,'Qual o melhor notebook de baixo custo?','Pessoal, eu preciso comprar um notebook urgente, mas não tenho muita grana. Eu sou estudante de Publicidade e sei que preciso de um bom processador pra usar os programas da Adobe (Embora ainda não saiba usá-los). DELL, Samsung, HP sei que são as melhores marcas, mas não disponho de muito dinheiro, como já disse. Qual outra marca com um bom processador vocês me indicam, de baixo custo?','2020-05-18 04:58:29',NULL,4,0),(17,'Como anunciar no waze?','Olá, sei que é possível anunciar no Waze, porém ainda está em fase Beta. Entrei em contato por email, como eles solicitam que seja feito mas não obtive retorno. Alguém sabe se existe alguma outra maneira? Obrigada.','2020-05-18 05:01:29',NULL,2,22),(18,'Tópico central: auxílio emergencial, caixa tem , está tendo dificuldades, poste aqui!','Eu li que o aplicativo para receber os R$ 600 do coronavoucher foi lançado hoje pela Caixa, mas não sei o nome dele. Alguém me passa o link?','2020-05-18 05:02:18',NULL,3,81),(24,'Testando','Test unitario 2','2020-05-19 23:48:52',NULL,3,0),(25,' como resolver problema de caps lock invertido?','Eu estou com um problema curioso no computador da empresa.\n\nA tecla do CAPS LOCK está invertida.\n\nCom o teclado com caps lock ativado, só sai os caracteres minúsculos. Com a tecla desativada [luz apagada], só aparecem os caracteres maiúsculos.\n\nDeveria ser o contrário, não? Como se resolve isso?','2020-05-19 23:53:43',NULL,4,3),(26,'Tópico central: auxílio emergencial, caixa tem , está tendo dificuldades, poste aqui!','Eu li que o aplicativo para receber os R$ 600 do coronavoucher foi lançado hoje pela Caixa, mas não sei o nome dele. Alguém me passa o link?','2020-05-20 00:31:01',NULL,3,0);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schemaversion`
--

DROP TABLE IF EXISTS `schemaversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schemaversion` (
  `version` bigint(20) NOT NULL,
  `name` text,
  `md5` text,
  `run_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schemaversion`
--

LOCK TABLES `schemaversion` WRITE;
/*!40000 ALTER TABLE `schemaversion` DISABLE KEYS */;
INSERT INTO `schemaversion` VALUES (0,NULL,NULL,'2020-05-14 03:47:16'),(1,'initialize_version_schema','ba258d6504ad9fdfb0d02bf9e32667cb','2020-05-14 03:47:17'),(2,'first_version','f204545fecc9de69b82373b9b9acbc8e','2020-05-14 03:47:17');
/*!40000 ALTER TABLE `schemaversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `created_in` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_in` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'velocidade','2020-05-14 03:47:17',NULL),(2,'wifi','2020-05-14 03:47:17',NULL),(3,'internet','2020-05-14 03:47:17',NULL),(4,'google play','2020-05-14 03:47:17',NULL),(5,'google play','2020-05-14 03:47:17',NULL),(6,'app','2020-05-14 03:47:17',NULL),(7,'ram','2020-05-14 03:47:17',NULL),(8,'celular','2020-05-14 03:47:17',NULL),(9,'backup','2020-05-14 03:47:17',NULL),(10,'recuperação','2020-05-14 03:47:17',NULL);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_question`
--

DROP TABLE IF EXISTS `tags_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_question` (
  `tag_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`question_id`),
  KEY `fk_tags_has_question_question1_idx` (`question_id`),
  KEY `fk_tags_has_question_tags1_idx` (`tag_id`),
  CONSTRAINT `fk_tags_has_question_question1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tags_has_question_tags1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_question`
--

LOCK TABLES `tags_question` WRITE;
/*!40000 ALTER TABLE `tags_question` DISABLE KEYS */;
INSERT INTO `tags_question` VALUES (1,1),(2,1),(1,2),(2,2),(3,3),(4,3),(3,4),(4,4),(5,5),(6,5),(1,9),(2,9),(1,11),(3,12),(4,12),(7,13),(8,13),(5,14),(6,14),(7,14),(8,14),(1,16),(2,16),(3,16),(7,17),(9,17),(10,17),(4,18),(6,18),(1,24),(2,24),(1,25),(2,25),(3,25),(9,26),(10,26);
/*!40000 ALTER TABLE `tags_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(100) NOT NULL,
  `birthday` date DEFAULT NULL,
  `cellphone` varchar(16) DEFAULT NULL,
  `created_in` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_in` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Bruno Machado','bruno.machado@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-14 03:47:17',NULL),(2,'Leticia Santos','leticia.santos@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-14 03:47:17',NULL),(3,'Daniele Costa','daniele.costa@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-14 03:47:17',NULL),(4,'Evandro Souza','evandro.souza@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-14 03:47:17',NULL),(5,'Francisco Almeida','francisco.almeida@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-14 03:47:17',NULL),(6,'Edson Silva','edson.silva@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-18 05:08:28',NULL),(7,'Katia Pereira','katia.pereira@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-18 05:08:28',NULL),(8,'Jessica Carvalho','jessica.carvalho@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-18 05:08:28',NULL),(9,'Ketlyn Ferreira','ketlyin.ferreira@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-18 05:08:28',NULL),(10,'Claudia Fonseca','claudia.fonseca@gmail.com','pbkdf2:sha256:150000$arV5xx1H$7ea9dce250cadc2df011bbfadc93321f2d28e7669f6f1d49b2c288f77cca261c',NULL,NULL,'2020-05-18 05:08:28',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'forumdb'
--

--
-- Dumping routines for database 'forumdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-19 21:35:10
