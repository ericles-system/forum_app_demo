Para utilizar o projeto no modo desenvolvimento é necessário instalar o seguinte pacote:

`npm install create-react-app`

Ou de forma global:

`npm install --global create-react-app`

Necessário configurar proxy reverso para o front se comunicar com o backend. Configuração do proxy para dev com npm está presente no arquivo `package.json`.

Configuração de proxy reverso para ambientes deve ser realizada dentro do nginx.

## Resolver problema com Hot Loader do Webpack

```sh
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```