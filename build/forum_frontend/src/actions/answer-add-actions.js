import axios from 'axios';

export const FETCH_ANSWER_ADD_BEGIN   = 'FETCH_ANSWER_ADD_BEGIN';
export const FETCH_ANSWER_ADD_SUCCESS = 'FETCH_ANSWER_ADD_SUCCESS';
export const FETCH_ANSWER_ADD_FAILURE = 'FETCH_ANSWER_ADD_FAILURE';
export const FETCH_ANSWER_ADD_RESET = 'FETCH_ANSWER_ADD_RESET';
export const FETCH_LIKE_QUESTION = 'FETCH_LIKE_QUESTION';
export const FETCH_LIKE_ANSWER = 'FETCH_LIKE_ANSWER';

const fetchAnswerAddBegin = () => ({
  type: FETCH_ANSWER_ADD_BEGIN
});

const fetchAnswerAddSuccess = question => ({
  type: FETCH_ANSWER_ADD_SUCCESS,
  payload: { question }
});

const fetchLikeQuestionSuccess = like => ({
  type: FETCH_LIKE_QUESTION,
  payload: like
});

const fetchLikeAnwserSuccess = like => ({
  type: FETCH_LIKE_ANSWER,
  payload: like
});

const fetchAnswerAddFailure = error => ({
  type: FETCH_ANSWER_ADD_FAILURE,
  payload: { error }
});

export const fetchResetAnswer = () => ({
  type: FETCH_ANSWER_ADD_RESET
});

export function fetchAnswerAdd(questionId, obj) {
  return dispatch => {
    dispatch(fetchAnswerAddBegin());
    return axios.post(`${process.env.REACT_APP_ANSWERS_URL.replace(':questionId', questionId)}`, obj )
    .then(response => {
        dispatch(fetchAnswerAddSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchAnswerAddFailure(error)));
  };
}

export function fetchLikeQuestionAdd(obj) {
  return dispatch => {
    dispatch(fetchAnswerAddBegin());
    return axios.put(`${process.env.REACT_APP_LIKES_URL}`, obj 
    ).then(response => {
        dispatch(fetchLikeQuestionSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchAnswerAddFailure(error)));
  };
}

export function fetchLikeAnswerAdd(obj) {
  return dispatch => {
    dispatch(fetchAnswerAddBegin());
    return axios.put(`${process.env.REACT_APP_LIKES_URL}`, obj 
    ).then(response => {
        dispatch(fetchLikeAnwserSuccess(response.data));
        return response.data;
      })
      .catch(error => dispatch(fetchAnswerAddFailure(error)));
  };
}
