export const INCREMENT_SERVICES_COUNTER = 'INCREMENT_SERVICES_COUNTER';
export const DECREMENT_SERVICES_COUNTER = 'DECREMENT_SERVICES_COUNTER';
export const FETCH_SERVICES_COUNTER = 'FETCH_SERVICES_COUNTER';

export const incrementServicesCounter = () => ({
  type: INCREMENT_SERVICES_COUNTER
});

export const decrementServicesCounter = () => ({
  type: DECREMENT_SERVICES_COUNTER
});

export const servicesCounter = () => ({
  type: FETCH_SERVICES_COUNTER
});