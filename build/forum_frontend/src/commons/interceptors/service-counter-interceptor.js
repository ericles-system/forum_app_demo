// import { Store } from "../../store";
// import { incrementServicesCounter, decrementServicesCounter } from "../../actions/service-counter-actions";
// import * as storage from '../../commons/storage/storage'
import axios from 'axios';

axios.defaults.timeout = 60 * 1 * 1000; // 1 min

// axios.interceptors.request.use(async (config) => {
//   if (storage.getData('login')) {
//     const token = storage.getData('login').token
//     if (token) {
//       config.headers['Authorization'] = token;
//     }    
//   }
//   config.headers['Content-Type'] = 'application/json';
//   Store.dispatch(incrementServicesCounter());
//   return config;
// }, (error) => {
//   console.error('Erro na saída do serviço', error);
//   Store.dispatch(decrementServicesCounter());
//   return Promise.reject(error.response);
// });

// axios.interceptors.response.use((response) => {
//   Store.dispatch(incrementServicesCounter());
//   return response;
// },(error) => {
//   console.error('Erro no retorno do serviço', error);
//   Store.dispatch(decrementServicesCounter());
//   return Promise.reject(error.response);
// });