import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Row, Col } from "react-bootstrap";
import * as moment from 'moment';
import { fetchLikeAnswerAdd } from '../../../actions/answer-add-actions';
import './answer-list.css'

const AnswerList = props => {

	const renderAnswerLike = (a) => {
		if (typeof props.likeAnswers === "object"){
			if (props.likeAnswers.answers.id === a.id){
				a.like = props.likeAnswers.answers.like
			}
		}

		return a.like
	}

	const handleLike = (a) => {
		props.fetchLikeAnswerAdd({type: "answers", id: a.id}).then(a => {
			renderAnswerLike(a)
		})
	}

    const renderRows = () => {
        let anwersList = props.listPaginated.map(a => (
			<Row className="summary">
				<Col sm={ 2 } className="count">
					<FontAwesomeIcon 
						className="like-icon"
						icon={ faThumbsUp } 
						size="2x"
						title="Curtir"
						onClick={ () => handleLike(a) }
					/>
					<span className="like">{ renderAnswerLike(a) }</span>
				</Col>
				<Col md={ 8 } className="summary-wrapper">
                    <p>
                        {a.text}
                    </p>
					<div className="userinfo">
						<span className="relativetime" title={moment(a.createdIn).toDate().toString('pt-BR')}>
							Respondeu: { moment(a.createdIn).toDate().toDateString('pt-BR') + " " }
                            { moment(a.createdIn).toDate().toLocaleTimeString('pt-BR')},
						</span>
						<Link className="add-link" to="#">
							{a.userName}
						</Link>
					</div>
				</Col>
			</Row>
        ));
    
        if (anwersList.length === 0) 
            anwersList.push(<h3>Nenhuma resposta encontrada</h3>);
        
        return anwersList
	}
	
	return (
		<div className="answers">
			<div className="answers-header">
				<h2 className="subtitle">
				{props.total} Respostas:
				</h2>
			</div>
			<div className="answers-body">
				{ renderRows() }
			</div>
		</div>
	)
};

const mapStateToProps = state => ({
	listPaginated: state.answerPaginated.answers,
	total: state.answerPaginated.total,
	likeAnswers: state.answer.likeAnswers
});

const mapDispatchToProps = dispatch => ({
    fetchLikeAnswerAdd: (obj) => dispatch(fetchLikeAnswerAdd(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AnswerList);