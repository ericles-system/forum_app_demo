import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { fetchAnswerAdd, fetchResetAnswer, fetchLikeQuestionAdd, FETCH_ANSWER_ADD_FAILURE } from '../../actions/answer-add-actions';
import { fetchAnswerPaginated } from '../../actions/answer-paginated-actions';
import * as moment from 'moment';
import * as Utils from '../../commons/utils.js';
import { Row, Col } from "react-bootstrap";
import Pagination from '../../commons/pagination/pagination';
import AwnswerList from './answer-list/answer-list';
import AnswerForm from './answer-form/answer-form'

import './answer.css';

class Answer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            questionId: null,
            answerTotal: 0,
            title: 'N/D',
            text: '',
            userName: '',
            like: 0,
            createdIn: moment.now(),
            tags: [],
            limit: parseInt(process.env.REACT_APP_GRID_RECORDS_PER_PAGE),
            page: 1
        }
    }

    renderQuestionLike = () => {
        var like = 0
        if (typeof this.props.likeQuestions === "object") {
            like = this.props.likeQuestions.questions.like
        } else {
            like = this.state.like
        }

        return like
    }

    componentDidMount() {
        window.scroll(0, 0)
        this.props.fetchResetAnswer()
        this.updateGrid()
    }

    gridFilter = {
        title: undefined
    };

    handleSave = (answer) => {

        const answerToSave = { ...answer };

        if (!answerToSave.text) {
            Utils.addNotification('Resposta não pode ser vazia', Utils.NotificationType.WARNING);
            return;
        }

        this.props.fetchAnswerAdd(this.state.questionId, answerToSave).then((response) => {
            if (response.type === FETCH_ANSWER_ADD_FAILURE) throw response.payload;
            Utils.addNotification('Resposta salva com sucesso!', Utils.NotificationType.SUCCESS);
            this.updateGrid()
        }).catch((response) => {
            Utils.addNotification('Falha ao salvar a resposta', Utils.NotificationType.DANGER);
        });
    };

    loadQuestion() {
        this.setState(state => {
            return {
                ...state,
                questionId: this.props.question.id,
                title: this.props.question.title,
                text: this.props.question.text,
                userName: this.props.question.userName,
                createdIn: this.props.question.createdIn,
                tags: this.props.question.tags,
                like: this.props.question.like,
                answerTotal: this.props.total
            }
        });
    }

    updateGrid = () => {
        this.props.fetchAnswerPaginated(this.props.match.params.questionId, this.state.page, this.state.limit, this.gridFilter).then(e => {
            this.loadQuestion()
        });
    };

    onChangePage = (pagination) => {
        // this.toggleSelectAllRows(false);
        this.setState(state => {
            return {
                ...state,
                page: pagination.page
            }

        }, () => { this.updateGrid() });
    };

    handleLike(type) {
        const obj = {
            id: this.state.questionId,
            type: type
        }
        this.props.fetchLikeQuestionAdd(obj)
        this.renderQuestionLike()
    }

    render() {

        const tags = this.state.tags.map(t => (
            <Link className="add-link bubble" to="#" key={t.tag_id}>
                {t.tag}
            </Link>
        ))

        const { limit, page, title, text, userName, createdIn } = this.state

        return (
            <div>
                <div className="questions">
                    <div className="questions-header">
                        <div className="row">
                            <div className="col-md-1">
                                <Link className="add-link" to="/home">
                                    <FontAwesomeIcon
                                        icon={faArrowLeft}
                                        size="3x"
                                        title="Voltar"
                                    />
                                </Link>
                            </div>
                            <div className="col-md-10">
                                <h1 className="subtitle">
                                    {title}
                                </h1>
                            </div>
                        </div>
                    </div>
                    <div className="question-body">
                        <Row className="summary">
                            <Col sm={2} className="count">
                                <FontAwesomeIcon
                                    className="like-icon"
                                    icon={faThumbsUp}
                                    size="2x"
                                    title="Curtir"
                                    onClick={() => this.handleLike("questions")}
                                />
                                <span className="like">
                                    {this.renderQuestionLike()}
                                </span>
                            </Col>
                            <Col sm={8} className="summary-wrapper">
                                <p> {text} </p>
                                <div className="userinfo">
                                    <span className="relativetime" title={moment(createdIn).toDate().toString('pt-BR')}>
                                        Perguntou: {moment(createdIn).toDate().toDateString('pt-BR') + " "}
                                        {moment(createdIn).toDate().toLocaleTimeString('pt-BR')},
                                        </span>
                                    <Link className="add-link" to="#">
                                        {userName}
                                    </Link>
                                </div>
                                <div className="tags">
                                    {tags}
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
                <AwnswerList question={{ ...this.state }} handleLike={this.handleLike} />
                <div className="container pagination-flex-left">
                    <Pagination
                        totalRecords={this.props.total}
                        pageLimit={limit}
                        pagesToShow={parseInt(process.env.REACT_APP_GRID_PAGINATION_MAX_PAGES_TO_SHOW)}
                        onChangePage={this.onChangePage}
                        page={page}
                    />
                </div>
                <AnswerForm handleSave={this.handleSave} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    answerLoading: state.answerPaginated.loading,
    answerError: state.answerPaginated.error,
    total: state.answerPaginated.total,
    question: state.answerPaginated.data,
    likeQuestions: state.answer.likeQuestions
});

const mapDispatchToProps = dispatch => ({
    fetchAnswerAdd: (questionId, answer) => dispatch(fetchAnswerAdd(questionId, answer)),
    fetchResetAnswer: () => dispatch(fetchResetAnswer()),
    fetchLikeQuestionAdd: (obj) => dispatch(fetchLikeQuestionAdd(obj)),
    fetchAnswerPaginated: (questionId, limit, page, search) => dispatch(fetchAnswerPaginated(questionId, limit, page, search))
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Answer));