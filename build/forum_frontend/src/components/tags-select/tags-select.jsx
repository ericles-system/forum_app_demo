import React, { Component } from 'react';
import * as Utils from '../../commons/utils';
import { fetchTags } from '../../actions/tags-action'
import { connect } from 'react-redux';
import ForumSelect from '../../commons/forum-select/forum-select';
import PropTypes from "prop-types";

class TagsSelect extends Component {

  componentDidMount() {
    this.props.fetchTags();
  }

  render() {

    if (this.props.tagsError) {
      Utils.addNotification('Ocorreu um erro ao carregar as Tags', Utils.NotificationType.DANGER);
    }

    let tagsList = [];
    if (this.props.tagsList && this.props.tagsList.length >= 0) {
      tagsList = this.props.tagsList.map(model => { return { value: model.id, label: model.name } });
    }

    return (
      <ForumSelect 
        options={ tagsList } 
        placeholder="TAGS" 
        searchable="true"
        { ...this.props }
        loading={ this.props.tagsLoading } 
        disabled={ this.props.disabled } />
    );
  }
}

TagsSelect.propTypes = {
  values: PropTypes.array
};

const mapStateToProps = state => ({
  tagsList: state.tags.data,
  tagsLoading: state.tags.loading,
  tagsError: state.tags.error,
});

const mapDispatchToProps = dispatch => ({
    fetchTags: () => dispatch(fetchTags())
});

export default connect(mapStateToProps, mapDispatchToProps)(TagsSelect);