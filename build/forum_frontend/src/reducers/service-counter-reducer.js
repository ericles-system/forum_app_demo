import { INCREMENT_SERVICES_COUNTER, DECREMENT_SERVICES_COUNTER } from "../actions/service-counter-actions";

const initialState = {
  count: 0
};

export default function serviceCounterReducer(state = initialState, action) {
  switch(action.type) {
    case INCREMENT_SERVICES_COUNTER:
      return {
        count: state.count + 1
      };
    case DECREMENT_SERVICES_COUNTER:
      return {
        count: state.count - 1
      };
    
    default:
      return state;
  }
}